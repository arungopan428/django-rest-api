from django.db import models


class Book(models.Model):
    book_name = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    count = models.IntegerField()
    updated_at = models.DateTimeField(auto_now=True)


		

