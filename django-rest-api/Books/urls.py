from django.urls import path
from . import views


urlpatterns = [
    path('', views.ListCreateBookAPIView.as_view(), name='get_post_bookd'),
    path('create-book/', views.CreateBookAPIView.as_view(), name='crate_book'),
    path('<int:pk>/', views.IndividualBookAPIView.as_view(), name='get_delete_update_book'),
]