
## Requirements
- Python 3.6
- Django 3.1
- Django REST Framework

## Installation
After you cloned the repository, you want to create a virtual environment, so you have a clean python installation.
You can do this by running the command
```
python -m venv env
```
auth token
register
http://127.0.0.1:8000/api/v1/auth/register/
get token
http http://127.0.0.1:8000/api/v1/auth/token/ username="username" password="password"


`books` | GET | READ | Get all books
`books/:id` | GET | READ | Get a single book
`books`| POST | CREATE | Create a new book
`books/:id` | PUT | UPDATE | Update a book
`books/:id` | DELETE | DELETE | Delete a book


### Commands
```
Get all books
http http://127.0.0.1:8000/api/v1/books/ "Authorization: Bearer {YOUR_TOKEN}" 
Get a single book
http GET http://127.0.0.1:8000/api/v1/books/{book_id}/ "Authorization: Bearer {YOUR_TOKEN}" 
Create a new book
http POST http://127.0.0.1:8000/api/v1/books/ "Authorization: Bearer {YOUR_TOKEN}" book_name="test" author="fdfg" count="10" 
update a book
http PUT http://127.0.0.1:8000/api/v1/books/{book_id}/ "Authorization: Bearer {YOUR_TOKEN}"
Delete a book
http DELETE http://127.0.0.1:8000/api/v1/books/{book_id}/ "Authorization: Bearer {YOUR_TOKEN}"
```


